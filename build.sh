#!/bin/bash -xe
rm -rf math-app.apk
quasar build -m capacitor -T android
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore release-key.keystore dist/capacitor/android/apk/release/app-release-unsigned.apk alias_name
$ANDROID_HOME/build-tools/29.0.2/zipalign -v 4 dist/capacitor/android/apk/release/app-release-unsigned.apk math-app.apk
